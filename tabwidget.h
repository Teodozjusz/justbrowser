#ifndef TABWIDGET_H
#define TABWIDGET_H

#include "settings.h"
#include "webview.h"

#include <QTabWidget>



class TabWidget : public QTabWidget
{
        Q_OBJECT
public:
    TabWidget(QWidget *parent = nullptr, Settings *aSettings = new Settings());

    WebView *webView(int index) const;
    WebView *currentWebView() const;
    WebView *newBackgroundBrowseTab(QString url, int place = 0);
    WebView *newBrowseTab(QString url, int place = 0);
    void closeTab(int index);
    void closeCurrentTab();
signals:

private slots:
private:
    void setShortcuts();
private:

    QScopedPointer<FullscreenWindow> p_fullscreenWindow;

    QString lastClosedTab;

    const QString newTabTitle = "Tabula rasa";
    Settings *settings;
};

#endif // TABWIDGET_H
