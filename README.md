**JustBrowser** - Just for browsing

The main goal of JustBrowser is to make web browsing as much effective and comfortable as possible by:
*  Limiting screenspace occupied by GUI to minimum
*  Using mouse to navigate on page and keyboard to everything else.
*  Allowing to configure almost everything by user.

Everything you need to start using JustBrowser is: OS based on Linux (JustBrowser is developed on Debian) and Qt >= 5.12.

Config file path: ~/.config/JustBrowser/config

Example config file can be downloaded from "Config" folder.
Compiled, ready to use executable file can be downloaded from "Compiled" folder.