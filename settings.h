﻿#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDir>
#include <QKeySequence>
#include <QList>



class Settings
{
public:
    Settings();
    QKeySequence getBackShortcut() const;

    QKeySequence getForwardShortcut() const;

    QKeySequence getRefreshShortcut() const;

    QKeySequence getStopShortcut() const;

    QKeySequence getNewPageShortcut() const;

    QKeySequence getNewTabShortcut() const;

    QKeySequence getCloseTabShortcut() const;

    QKeySequence getResetZoomShortcut() const;

    QKeySequence getZoomOutShortcut() const;

    QKeySequence getZoomInShortcut() const;

    QKeySequence getScrollToTop() const;

    QList<QKeySequence> getSwitchTab() const;

    bool getShowScroll() const;

    QStringList getStartPages() const;

    bool getForceDarkTheme() const;

    QList<QKeySequence> getMoveTab() const;

    bool getAutoHideTabBar() const;


    QMap<QString, QString> getQuickBookmarks() const;

    bool getOpenQuickmarksInNewTab() const;

    QString getDefaultDownloadPath() const;

    QKeySequence getMuteShortcut() const;

    QString getDefaultSearchEngine() const;

    QList<int> getNewTabColor() const;

    QKeySequence getPreviousTab() const;

    QKeySequence getNextTab() const;

    QKeySequence getRestoreLastClosedTab() const;

private:
    QKeySequence backShortcut = QKeySequence();
    QKeySequence forwardShortcut = QKeySequence();
    QKeySequence refreshShortcut = QKeySequence();
    QKeySequence stopShortcut = QKeySequence();
    QKeySequence newPageShortcut = QKeySequence();
    QKeySequence muteTabShortcut = QKeySequence();

    QKeySequence newTabShortcut = QKeySequence();
    QKeySequence closeTabShortcut = QKeySequence();
    QKeySequence restoreLastClosedTab = QKeySequence();
    QList<QKeySequence> switchTab;
    QList<QKeySequence> moveTab;
    QKeySequence previousTab = QKeySequence();
    QKeySequence nextTab = QKeySequence();

    QKeySequence zoomInShortcut = QKeySequence();
    QKeySequence zoomOutShortcut = QKeySequence();
    QKeySequence resetZoomShortcut = QKeySequence();

    QKeySequence scrollToTop = QKeySequence();

    QStringList startPages;

    bool showScroll = false;
    bool forceDarkTheme = false;
    bool autoHideTabBar = false;
    QList<int> newTabColor = {0,0,0};

    bool openQuickmarksInNewTab = false;
    QString defaultDownloadPath = "NULL";
    QString defaultSearchEngine = "https://duckduckgo.com/?q=";

    QMap<QString, QString> quickBookmarks;
private:
    QDir moveToDir(QDir dir, QString targetDir);
};

#endif // SETTINGS_H
