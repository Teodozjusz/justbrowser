#include "changepagedialog.h"
#include "ui_changepagedialog.h"

#include <QAction>
#include <QMessageBox>

ChangePageDialog::ChangePageDialog(QWidget *parent, Settings *aSettings) :
    QDialog(parent),
    ui(new Ui::ChangePageDialog),
    settings(aSettings)
{
    ui->setupUi(this);
    ui->addressEdit->setFocus();
	ui->addressEdit->selectAll();
    ui->horizontalLayout->setMargin(0);

    QAction *closeAction = new QAction();
    closeAction->setShortcut(QKeySequence("Escape"));
    connect(closeAction, &QAction::triggered, this, &ChangePageDialog::close);
    this->addAction(closeAction);
}

ChangePageDialog::~ChangePageDialog()
{
    delete ui;
}

void ChangePageDialog::on_OKBtn_clicked()
{
    if(ui->addressEdit->text().trimmed() != "") {
        url = ui->addressEdit->text();
        if (url.indexOf(".") != -1 || (url.indexOf(".") != -1 && url.indexOf("/") != -1)) {
            if (!url.startsWith("http://") && !url.startsWith("https://")) url = "http://" + url;
        }
        else {
            if(url.startsWith("yt:")) {
                url.remove(0,3);
                url = "https://www.youtube.com/results?search_query=" + url;
            }
            else if(url.startsWith("wiki:")) {
                url.remove(0,5);
                url = "https://pl.wikipedia.org/wiki/" + url;
            }
            else {
                url = settings->getDefaultSearchEngine() + url;
            }
        }
        success = true;
        close();
    }
    else {
        QMessageBox::warning(this, "Error", "Address cannot be empty or contain only spaces!");
        ui->addressEdit->clear();
    }
}

QString ChangePageDialog::getUrl() const
{
    return url;
}

void ChangePageDialog::setUrl(QString url)
{
    ui->addressEdit->setText(url);
}

bool ChangePageDialog::getSuccess() const
{
    return success;
}
