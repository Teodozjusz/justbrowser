#include "fullscreenwindow.h"
#include "ui_fullscreenwindow.h"

#include <QShortcut>
#include <QWebEngineSettings>

FullscreenWindow::FullscreenWindow(QWebEngineView* oldWeb, bool hideParent, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::FullscreenWindow),
    m_oldWeb(oldWeb)
{
    web->settings()->setAttribute(QWebEngineSettings::FullScreenSupportEnabled, true);
    web->setPage(oldWeb->page());

    m_oldWeb->setHtml("<html><title>" + web->title() + "</title><p align='center'>Ta karta działa w trybie Picture-in-Picture</p><html>");
    setCentralWidget(web);
    if(hideParent) {
        m_oldWeb->window()->hide();
    }
    showFullScreen();

    new QShortcut(QKeySequence("Esc"), this, SLOT(close()));
    new QShortcut(QKeySequence("Ctrl+Space"), this, SLOT(switchPIP()));
}

FullscreenWindow::~FullscreenWindow()
{
    m_oldWeb->triggerPageAction(QWebEnginePage::ExitFullScreen);
    m_oldWeb->setPage(web->page());
    m_oldWeb->window()->show();
    delete ui;
}

void FullscreenWindow::switchPIP()
{
    web->settings()->setAttribute(QWebEngineSettings::FullScreenSupportEnabled, true);
    if(this->isFullScreen()) {
        showNormal();
    }
    else showFullScreen();
}
