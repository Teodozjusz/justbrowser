#ifndef FULLSCREENWINDOW_H
#define FULLSCREENWINDOW_H

#include <QMainWindow>
#include <QWebEngineView>

namespace Ui {
class FullscreenWindow;
}

class FullscreenWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit FullscreenWindow(QWebEngineView* oldWeb, bool startInPIP, QWidget *parent = nullptr);
    ~FullscreenWindow();
private slots:
    void switchPIP();
private:
    Ui::FullscreenWindow *ui;
    QWebEngineView* web = new QWebEngineView();
    QWebEngineView* m_oldWeb;
};

#endif // FULLSCREENWINDOW_H
