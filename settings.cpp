#include "settings.h"

#include <QCoreApplication>
#include <QDir>
#include <QTextStream>
#include <QDebug>

Settings::Settings()
{
    for(int i = 0; i < 9; i++) {
        switchTab.append(QKeySequence());
    }
    for(int i = 0; i < 9; i++) {
        moveTab.append(QKeySequence());
    }

    QDir dir(QDir::homePath());
    dir = moveToDir(dir, ".config");
    dir = moveToDir(dir, "JustBrowser");
    QFile file(dir.absolutePath() + "/config");
    if(file.open(QFile::ReadOnly | QFile::Text)) {
        QTextStream stream(&file);
        while(!stream.atEnd()) {
            QStringList line = stream.readLine().split(" ");
            if(line.count() > 1) {
                if(line.at(0) == "Back") backShortcut = QKeySequence(line.at(1));
                else if(line.at(0) == "Forward") forwardShortcut = QKeySequence(line.at(1));
                else if(line.at(0) == "Refresh") refreshShortcut = QKeySequence(line.at(1));
                else if(line.at(0) == "Stop") stopShortcut = QKeySequence(line.at(1));
                else if(line.at(0) == "NewPage") newPageShortcut = QKeySequence(line.at(1));

                else if(line.at(0) == "NewTab") newTabShortcut = QKeySequence(line.at(1));
                else if(line.at(0) == "CloseTab") closeTabShortcut = QKeySequence(line.at(1));
                else if(line.at(0) == "RestoreLastClosedTab") restoreLastClosedTab = QKeySequence(line.at(1));

                else if(line.at(0) == "PreviousTab") previousTab = QKeySequence(line.at(1));
                else if(line.at(0) == "NextTab") nextTab = QKeySequence(line.at(1));

                else if(line.at(0) == "ZoomIn") zoomInShortcut = QKeySequence(line.at(1));
                else if(line.at(0) == "ZoomOut") zoomOutShortcut = QKeySequence(line.at(1));
                else if(line.at(0) == "ResetZoom") resetZoomShortcut = QKeySequence(line.at(1));

                else if(line.at(0) == "ScrollToTop") scrollToTop = QKeySequence(line.at(1));

                else if(line.at(0) == "MuteTab") muteTabShortcut = QKeySequence(line.at(1));

                else if(line.at(0) == "SwitchTab" && line.count() > 2) {
                    if(line.at(1).toInt() - 1 > 8) {
                        switchTab.insert(line.at(1).toInt() - 1, QKeySequence(line.at(2)));
                    }

                    else switchTab.replace(line.at(1).toInt() - 1, QKeySequence(line.at(2)));
                }

                else if(line.at(0) == "MoveTab" && line.count() > 2) {
                    if(line.at(1).toInt() - 1 > 8) {
                        moveTab.insert(line.at(1).toInt() - 1, QKeySequence(line.at(2)));
                    }
                    else moveTab.replace(line.at(1).toInt() - 1, QKeySequence(line.at(2)));
                }

                else if(line.at(0) == "ShowScroll") {
                    if(line.at(1) == "true") showScroll = true;
                }

                else if(line.at(0) == "StartPage") {
                    QString url = line.at(1);
                    if(!url.startsWith("http")) url = "http://" + url;
                    startPages.append(url);
                }
                else if(line.at(0) == "ForceDarkTheme") {
                    if(line.at(1) == "true") forceDarkTheme = true;
                }
                else if(line.at(0) == "AutoHideTabBar") {
                    if(line.at(1) == "true") autoHideTabBar = true;
                }
                else if(line.at(0) == "OpenQuickmarksInNewTab") {
                    if(line.at(1) == "true") openQuickmarksInNewTab = true;
                }

                else if(line.at(0) == "Quickmark" && line.count() > 2) {
                    QString url = line.at(1);
                    if (!url.startsWith("http")) url = "http://" + url;
                    quickBookmarks.insert(line.at(2), url);
                }

                else if(line.at(0) == "DefaultDownloadPath") {
                    defaultDownloadPath = line.at(1);
                    for(int i = 2; i < line.count(); i++){
                        defaultDownloadPath += " " + line.at(i);
                    }
                }

                else if(line.at(0) == "DefaultSearchEngine") {
                    defaultSearchEngine = line.at(1);
                    if(!defaultSearchEngine.startsWith("http")) defaultSearchEngine = "http://" + defaultSearchEngine;
                }

                else if(line.at(0) == "NewTabColor" && line.count() >= 4) {
                    newTabColor.clear();
                    newTabColor.append(line.at(1).toInt());
                    newTabColor.append(line.at(2).toInt());
                    newTabColor.append(line.at(3).toInt());
                }
            }
        }
    }
    if(startPages.isEmpty()) startPages.append("http://start.duckduckgo.com");
}

QDir Settings::moveToDir(QDir dir, QString targetDir) {
    if (dir.exists(targetDir)) {
       dir.cd(targetDir);
    }
    else{
       dir.mkdir(targetDir);
       dir.cd(targetDir);
    }
    return dir;
}

QKeySequence Settings::getBackShortcut() const
{
    return backShortcut;
}

QKeySequence Settings::getForwardShortcut() const
{
    return forwardShortcut;
}

QKeySequence Settings::getRefreshShortcut() const
{
    return refreshShortcut;
}

QKeySequence Settings::getStopShortcut() const
{
    return stopShortcut;
}

QKeySequence Settings::getNewTabShortcut() const
{
    return newTabShortcut;
}

QKeySequence Settings::getCloseTabShortcut() const
{
    return closeTabShortcut;
}

QKeySequence Settings::getNewPageShortcut() const
{
    return newPageShortcut;
}

bool Settings::getShowScroll() const
{
    return showScroll;
}

QStringList Settings::getStartPages() const
{
    return startPages;
}

bool Settings::getForceDarkTheme() const
{
    return forceDarkTheme;
}

QList<QKeySequence> Settings::getMoveTab() const
{
    return moveTab;
}

bool Settings::getAutoHideTabBar() const
{
    return autoHideTabBar;
}

QMap<QString, QString> Settings::getQuickBookmarks() const
{
    return quickBookmarks;
}

bool Settings::getOpenQuickmarksInNewTab() const
{
    return openQuickmarksInNewTab;
}

QString Settings::getDefaultDownloadPath() const
{
    return defaultDownloadPath;
}

QKeySequence Settings::getMuteShortcut() const
{
    return muteTabShortcut;
}

QString Settings::getDefaultSearchEngine() const
{
    return defaultSearchEngine;
}

QList<int> Settings::getNewTabColor() const
{
    return newTabColor;
}

QKeySequence Settings::getPreviousTab() const
{
    return previousTab;
}

QKeySequence Settings::getNextTab() const
{
    return nextTab;
}

QKeySequence Settings::getRestoreLastClosedTab() const
{
    return restoreLastClosedTab;
}

QKeySequence Settings::getScrollToTop() const
{
    return scrollToTop;
}

QKeySequence Settings::getResetZoomShortcut() const
{
    return resetZoomShortcut;
}

QKeySequence Settings::getZoomOutShortcut() const
{
    return zoomOutShortcut;
}

QKeySequence Settings::getZoomInShortcut() const
{
    return zoomInShortcut;
}

QList<QKeySequence> Settings::getSwitchTab() const
{
    return switchTab;
}
