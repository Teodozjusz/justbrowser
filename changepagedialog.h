#ifndef CHANGEPAGEDIALOG_H
#define CHANGEPAGEDIALOG_H

#include "settings.h"

#include <QDialog>

namespace Ui {
class ChangePageDialog;
}

class ChangePageDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ChangePageDialog(QWidget *parent = nullptr, Settings *aSettings = nullptr);
    ~ChangePageDialog();

    bool getSuccess() const;

    QString getUrl() const;

    void setUrl(QString url);

private slots:
    void on_OKBtn_clicked();

private:
    Ui::ChangePageDialog *ui;
    QString url;
    bool success = false;

    Settings *settings;
};

#endif // CHANGEPAGEDIALOG_H
