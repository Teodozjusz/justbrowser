#ifndef DOWNLOADITEM_H
#define DOWNLOADITEM_H

#include <QWebEngineDownloadItem>
#include <QWidget>

namespace Ui {
class DownloadItem;
}

class DownloadItem : public QWidget
{
    Q_OBJECT

public:
    explicit DownloadItem(QString file,  QWidget *parent = nullptr);
    ~DownloadItem();

public slots:
    void update(qint64 bytesReceived, qint64 bytesTotal);
    void destroyItem();
signals:
    void stopDownload();
    void pauseDownload();
    void resumeDownload();

private slots:
    void on_StopBtn_clicked();

    void on_pauseResumeBtn_clicked();

private:
    Ui::DownloadItem *ui;

    qint64 lastReceived = 0;

    double format(double number);
};

#endif // DOWNLOADITEM_H
