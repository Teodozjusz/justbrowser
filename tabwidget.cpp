#include "mainwindow.h"
#include "tabwidget.h"
#include "webview.h"

#include <QMessageBox>
#include <QTabBar>
#include <QWebEngineFullScreenRequest>

TabWidget::TabWidget(QWidget *parent, Settings *aSettings) :
    QTabWidget(parent),
    settings(aSettings)
{
    this->setTabBarAutoHide(settings->getAutoHideTabBar());
    this->setMovable(true);

    QTabBar *tabBar = this->tabBar();
    tabBar->setElideMode(Qt::ElideRight);
    tabBar->setDocumentMode(true);

    setShortcuts();
}

void TabWidget::setShortcuts()
{
    QAction *backAction = new QAction();
    QAction *forwardAction = new QAction();
    QAction *reloadAction = new QAction();
    QAction *stopAction = new QAction();
    QAction *goAction = new QAction();
    QAction *zoomInAction = new QAction();
    QAction *zoomOutAction = new QAction();
    QAction *resetZoomAction = new QAction();
    QAction *scrollToTop = new QAction();
    QAction *newTabAction = new QAction();
    QAction *closeTabAction = new QAction();
    QAction *resoreLastClosedTab = new QAction();
    QAction *nextTabAction = new QAction();
    QAction *previousTabAction = new QAction();
    QAction *muteTabAction = new QAction();

    backAction->setShortcut(settings->getBackShortcut());
    forwardAction->setShortcut(settings->getForwardShortcut());
    reloadAction->setShortcut(settings->getRefreshShortcut());
    stopAction->setShortcut(settings->getStopShortcut());
    goAction->setShortcut(settings->getNewPageShortcut());
    zoomInAction->setShortcut(settings->getZoomInShortcut());
    zoomOutAction->setShortcut(settings->getZoomOutShortcut());
    resetZoomAction->setShortcut(settings->getResetZoomShortcut());
    scrollToTop->setShortcut(settings->getScrollToTop());
    newTabAction->setShortcut(settings->getNewTabShortcut());
    closeTabAction->setShortcut(settings->getCloseTabShortcut());
    resoreLastClosedTab->setShortcut(settings->getRestoreLastClosedTab());
    nextTabAction->setShortcut(settings->getNextTab());
    previousTabAction->setShortcut(settings->getPreviousTab());
    muteTabAction->setShortcut(settings->getMuteShortcut());

    connect(backAction, &QAction::triggered, [this](){ currentWebView()->back(); });
    connect(forwardAction, &QAction::triggered, [this](){ currentWebView()->forward(); });
    connect(reloadAction, &QAction::triggered, [this](){ currentWebView()->reload(); });
    connect(stopAction, &QAction::triggered, [this](){ currentWebView()->stop(); });
    connect(goAction, &QAction::triggered, [this](){ currentWebView()->loadNewPage(); });
    connect(zoomInAction, &QAction::triggered, [this] () {
       this->currentWebView()->setZoomFactor(this->currentWebView()->zoomFactor() + 0.25);
    });
    connect(zoomOutAction, &QAction::triggered, [this] () {
       this->currentWebView()->setZoomFactor(this->currentWebView()->zoomFactor() - 0.25);
    });
    connect(resetZoomAction, &QAction::triggered, [this] () {
       this->currentWebView()->setZoomFactor(1);
    });
    connect(scrollToTop, &QAction::triggered, [this] () {
        this->currentWebView()->page()->runJavaScript(QString("window.scrollTo(%1, %2);").arg(0).arg(0));
    });
    connect(muteTabAction, &QAction::triggered, [this] () {
        if(!this->currentWebView()->page()->isAudioMuted()) {
            this->currentWebView()->page()->setAudioMuted(true);
        }
        else {
            this->currentWebView()->page()->setAudioMuted(false);
        }
    });

    connect(newTabAction, &QAction::triggered, [this]() { newBrowseTab("", 0); });
    connect(closeTabAction, &QAction::triggered, this, &TabWidget::closeCurrentTab);
    connect(resoreLastClosedTab, &QAction::triggered, [this](){
        if (lastClosedTab.trimmed() != "") this->newBrowseTab(lastClosedTab);
        else QMessageBox::information(this, "JustBrowser", "There is no tab to restore.");
    });
    connect(nextTabAction, &QAction::triggered, [this] () {
        if (this->currentIndex() < this->count() - 1) {
            this->setCurrentIndex(this->currentIndex() + 1);
        }
    });
    connect(previousTabAction, &QAction::triggered, [this] () {
        if (this->currentIndex() > 0) {
            this->setCurrentIndex(this->currentIndex() - 1);
        }
    });

    this->addAction(backAction);
    this->addAction(forwardAction);
    this->addAction(reloadAction);
    this->addAction(stopAction);
    this->addAction(goAction);
    this->addAction(zoomInAction);
    this->addAction(zoomOutAction);
    this->addAction(resetZoomAction);
    this->addAction(scrollToTop);
    this->addAction(newTabAction);
    this->addAction(closeTabAction);
    this->addAction(resoreLastClosedTab);
    this->addAction(nextTabAction);
    this->addAction(previousTabAction);
    this->addAction(muteTabAction);

    for(int i = 0; i < settings->getSwitchTab().count(); i++) {
        QAction *switchAction = new QAction;
        switchAction->setShortcut(settings->getSwitchTab().at(i));
        connect(switchAction, &QAction::triggered, [this, i](){ this->setCurrentIndex(i); });
        addAction(switchAction);
    }


    for(int i = 0; i < settings->getMoveTab().count(); i++) {
        QAction *moveAction = new QAction;
        moveAction->setShortcut(settings->getMoveTab().at(i));
        connect(moveAction, &QAction::triggered, [this, i]() {this->tabBar()->moveTab(this->currentIndex(), i); });
        addAction(moveAction);
    }

    QMap<QString, QString> bookmarks = settings->getQuickBookmarks();
    QMapIterator<QString, QString> iter(bookmarks);
    while(iter.hasNext()) {
        iter.next();
        QAction *bookmark = new QAction();
        QKeySequence keystroke = QKeySequence(iter.key());
        bookmark->setShortcut(keystroke);
        QString url = iter.value();
        connect(bookmark, &QAction::triggered, [this, url] () {
            if(this->webView(this->currentIndex())->title() != newTabTitle && settings->getOpenQuickmarksInNewTab()) {
                this->newBrowseTab(url);
            }
            else this->webView(currentIndex())->load(QUrl(url));
        });
        this->addAction(bookmark);
    }
}



WebView *TabWidget::webView(int index) const
{
    return qobject_cast<WebView*>(widget(index));
}

WebView *TabWidget::currentWebView() const
{
    return webView(this->currentIndex());
}

WebView *TabWidget::newBackgroundBrowseTab(QString url, int place)
{
    WebView *web = new WebView(this, url, settings);
    connect(web, &WebView::titleChanged, [this, web] (const QString &title) {
        QString titleCut = title;
        if(titleCut.length() > 40) {
            titleCut.truncate(40);
        }
        this->setTabText(indexOf(web), titleCut);
        MainWindow *mainWindow = qobject_cast<MainWindow*>(window());
        mainWindow->setWindowTitle(titleCut + " - JustBrowser");
    });
    connect(web, &WebView::iconChanged, [this, web] (const QIcon &icon) {
        this->setTabIcon(indexOf(web), icon);
    });
    connect(web->page(), &QWebEnginePage::fullScreenRequested, [this, web](QWebEngineFullScreenRequest request) {
        if (request.toggleOn()) {
            if (p_fullscreenWindow) return;
            request.accept();
            if(this->count() == 1) {
                p_fullscreenWindow.reset(new FullscreenWindow(web, true));
            }
            else {
                p_fullscreenWindow.reset(new FullscreenWindow(web, false));
            }
        }
        else {
            if (!p_fullscreenWindow) return;
            request.accept();
            p_fullscreenWindow.reset();
        }
    });
    connect(web->page(), &QWebEnginePage::featurePermissionRequested, [web] (const QUrl &securityOrigin,
            QWebEnginePage::Feature feature) {
        QMessageBox* message = new QMessageBox();
        message->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        message->setWindowTitle("Permission request.");
        message->setIcon(QMessageBox::Question);
        if(feature == QWebEnginePage::Geolocation)
            message->setText("Page " + securityOrigin.host() + " wants to know your location");
        else if (feature == QWebEnginePage::MediaAudioCapture)
            message->setText("Page " + securityOrigin.host() + " wants to capture audio.");
        else if (feature == QWebEnginePage::MediaVideoCapture)
            message->setText("Page " + securityOrigin.host() + " wants to capture video.");
        else if (feature == QWebEnginePage::MediaAudioVideoCapture)
            message->setText("Page " + securityOrigin.host() + " wants to capture audio and video.");
        else if (feature == QWebEnginePage::MouseLock)
            message->setText("Page " + securityOrigin.host() + " wants to lock your mouse.");
        else if (feature == QWebEnginePage::DesktopVideoCapture)
            message->setText("Page " + securityOrigin.host() + " wants to capture your screen.");
        else if (feature == QWebEnginePage::DesktopAudioVideoCapture)
            message->setText("Page " + securityOrigin.host() + " wants to capture your screen (with sound).");

        int decision = message->exec();
        switch(decision) {
        case QMessageBox::Yes: {
            web->page()->setFeaturePermission(securityOrigin, feature, QWebEnginePage::PermissionGrantedByUser);
            break;
        }
        case QMessageBox::No: {
            web->page()->setFeaturePermission(securityOrigin, feature, QWebEnginePage::PermissionDeniedByUser);
            break;
        }
        }
    });

    if(place != 0) insertTab(place, web, newTabTitle);
    else addTab(web, newTabTitle);
    web->show();
    return web;
}

WebView *TabWidget::newBrowseTab(QString url, int place)
{
    WebView *view = newBackgroundBrowseTab(url, place);
    this->setCurrentWidget(view);
    return view;
}

void TabWidget::closeTab(int index)
{
    QString tabURL = this->webView(index)->url().toString();
    if (tabURL.trimmed() != "") lastClosedTab = tabURL;

    if (index > 0) this->setCurrentIndex(index - 1);
    this->webView(index)->~WebView();
    if(this->count() == 0) {
        this->window()->close();
    }
}

void TabWidget::closeCurrentTab()
{
    closeTab(this->currentIndex());
}
