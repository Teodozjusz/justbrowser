#include "mainwindow.h"
#include <QApplication>
#include <iostream>

int main(int argc, char *argv[])
{
    std::cout << "JustBrowser - simple, keyboard focused web browser." << std::endl
              << "Written in 2018-2019 by Adam Bem in C++ and Qt5" << std::endl;
    QApplication a(argc, argv);
    QString startUrl = nullptr;
    if(argc > 1) {
        startUrl = argv[1];
        if(!startUrl.startsWith("http")) startUrl = "http://" + startUrl;

    }
    MainWindow w(nullptr, true, startUrl);
    w.show();

    return a.exec();
}
