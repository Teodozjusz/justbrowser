#ifndef WEBVIEW_H
#define WEBVIEW_H

#include <QAction>
#include <QWebEngineFullScreenRequest>
#include <QWebEngineView>
#include "fullscreenwindow.h"
#include "settings.h"

class WebView : public QWebEngineView
{
        Q_OBJECT
public:
    WebView(QWidget *parent = nullptr, QString url = "", Settings *aSettings = new Settings());
    WebView webView(int index) const;
    void loadNewPage();
signals:
private slots:

protected:
    QWebEngineView *createWindow(QWebEnginePage::WebWindowType type) override;
private:

private:
    QAction *backAction = new QAction();
    QAction *forwardAction = new QAction();
    QAction *reloadAction = new QAction();
    QAction *stopAction = new QAction();
    QAction *goAction = new QAction();

    QAction *zoomInAction = new QAction();
    QAction *zoomOutAction = new QAction();
    QAction *resetZoomAction = new QAction();

    QAction *scrollToTop = new QAction();

    Settings *browserSettings;
};

#endif // WEBVIEW_H
