#include "changepagedialog.h"
#include "webview.h"
#include "fullscreenwindow.h"
#include "mainwindow.h"

#include <QAction>
#include <QMessageBox>
#include <QShortcut>
#include <QWebEngineFullScreenRequest>
#include <QWebEngineSettings>

WebView::WebView(QWidget *parent, QString url, Settings *aSettings) :
    QWebEngineView(parent),
    browserSettings(aSettings)
{
    settings()->setAttribute(QWebEngineSettings::FullScreenSupportEnabled, true);
    settings()->setAttribute(QWebEngineSettings::ShowScrollBars, browserSettings->getShowScroll());
    connect(this, &WebView::loadStarted, [this](){
        this->page()->setBackgroundColor(Qt::white);
    });

    if(url.trimmed() != "") {
        load(QUrl(url));
    }

    QColor color;
    color.setRgb(browserSettings->getNewTabColor().at(0),
                 browserSettings->getNewTabColor().at(1),
                 browserSettings->getNewTabColor().at(2));
    page()->setBackgroundColor(color);
}

void WebView::loadNewPage()
{
    ChangePageDialog *dialog = new ChangePageDialog(this, browserSettings);
    if(title() != "Tabula rasa") dialog->setUrl(url().toString());
    dialog->exec();
    if(dialog->getSuccess()) {
        load(QUrl(dialog->getUrl()));
    }

}

QWebEngineView *WebView::createWindow(QWebEnginePage::WebWindowType type)
{
    MainWindow *mainWindow = qobject_cast<MainWindow*>(window());
    int place = mainWindow->tabWidget->currentIndex() + 1;
    if (!mainWindow)
        return nullptr;

    switch (type) {
    case QWebEnginePage::WebBrowserTab: {
        return mainWindow->tabWidget->newBrowseTab("", place);
    }
    case QWebEnginePage::WebBrowserBackgroundTab: {
        return mainWindow->tabWidget->newBackgroundBrowseTab("", place);
    }
    case QWebEnginePage::WebBrowserWindow: {
        MainWindow *window = new MainWindow(nullptr, false);
        return window->tabWidget->newBrowseTab("", 0);
    }
    case QWebEnginePage::WebDialog: {
        return mainWindow->tabWidget->newBrowseTab("", place);
    }
    }
    return nullptr;
}

