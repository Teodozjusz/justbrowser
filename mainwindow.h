#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "settings.h"
#include "tabwidget.h"

#include <QMainWindow>
#include <QShortcut>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr, bool defaultNewTab = true, QString startUrl = nullptr);
    ~MainWindow();

    TabWidget *tabWidget;

private slots:

    void on_tabWidget_tabBarDoubleClicked(int index);

    void selectTab(int index);

    void on_tabWidget_currentChanged(int index);

    void downloadRequested(QWebEngineDownloadItem *download);

private:
    Ui::MainWindow *ui;
    QList<QShortcut> tabShortcut;
    bool isReady = false;



    Settings *settings;

    QString lastDownloadPath = "NULL";

private:
    void setShortcuts();
};

#endif // MAINWINDOW_H
