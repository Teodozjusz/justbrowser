#include "downloaditem.h"
#include "ui_downloaditem.h"
#include <cmath>

DownloadItem::DownloadItem(QString file, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DownloadItem)
{
    ui->setupUi(this);
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    ui->fileNameLabel->setText(file);

    ui->speedLabel->setVisible(false);
}

DownloadItem::~DownloadItem()
{
    delete ui;
}

void DownloadItem::update(qint64 bytesReceived, qint64 bytesTotal)
{
    if (bytesTotal != 0){
        int kB = 1000;
        int MB = 1000000;
        int GB = 1000000000;
        if (bytesTotal > GB) { // Too much numbers after "."
            ui->progressLabel->setText(QString::number(format( (double)bytesReceived/GB) ) + "GB/" + QString::number(bytesTotal/GB) + "GB"
                                    "(" + QString::number((bytesReceived*100)/bytesTotal) +"%)");
        }
        else if (bytesTotal> MB) {
            ui->progressLabel->setText(QString::number(bytesReceived/MB) + "MB/" + QString::number(bytesTotal/MB) + "MB"
                                    "(" + QString::number((bytesReceived*100)/bytesTotal) +"%)");
        }
        else if (bytesTotal > kB) {
            ui->progressLabel->setText(QString::number(bytesReceived/kB) + "kB/" + QString::number(bytesTotal/kB) + "kB"
                                    "(" + QString::number((bytesReceived*100)/bytesTotal) +"%)");
        }
    }

}

void DownloadItem::destroyItem()
{
    delete ui;
}

void DownloadItem::on_StopBtn_clicked()
{
    emit stopDownload();
}

void DownloadItem::on_pauseResumeBtn_clicked()
{
    if(ui->pauseResumeBtn->text() == "Pause") {
        emit pauseDownload();
        ui->pauseResumeBtn->setText("Resume");
    }
    else {
        emit resumeDownload();
        ui->pauseResumeBtn->setText("Pause");
    }
}

double DownloadItem::format(double number)
{
    return round(number*100)/100;
}
