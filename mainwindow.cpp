#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QWebEngineView>
#include <QMessageBox>
#include <QDebug>
#include <QShortcut>
#include <QWebEngineProfile>
#include <QFileDialog>
#include <QProgressBar>
#include <QSizePolicy>
#include <downloaditem.h>
#include <QVBoxLayout>
#include <QVBoxLayout>
#include <QDockWidget>

MainWindow::MainWindow(QWidget *parent, bool defaultNewTab, QString startUrl) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    if (startUrl != nullptr) defaultNewTab = false;
    settings = new Settings();
    tabWidget = new TabWidget(this, settings);

    if(settings->getForceDarkTheme()) {
        this->setStyleSheet("background-color: black;"
                            "color: white;");
        tabWidget->setStyleSheet("background-color: #0F0F0F; "
                                 "color: white;");
    }
    ui->toolBar->setVisible(false);

    connect(tabWidget, SIGNAL(currentChanged(int)), this, SLOT(on_tabWidget_currentChanged(int)));
    connect(tabWidget, SIGNAL(tabBarDoubleClicked(int)), this, SLOT(on_tabWidget_tabBarDoubleClicked(int)));
    connect(QWebEngineProfile::defaultProfile(), &QWebEngineProfile::downloadRequested, this, &MainWindow::downloadRequested);

    ui->verticalLayout->addWidget(tabWidget);

    isReady = true;
    if(defaultNewTab) {
        foreach(QString page, settings->getStartPages()) {
            tabWidget->newBrowseTab(page);
        }
    }
    else if(startUrl != nullptr) {
        tabWidget->newBrowseTab(startUrl);
    }
    this->resize(1000, 600);
    showMaximized();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_tabWidget_tabBarDoubleClicked(int index)
{
    tabWidget->closeTab(index);
}

void MainWindow::selectTab(int index) {
    tabWidget->setCurrentIndex(index);
}

void MainWindow::on_tabWidget_currentChanged(int index)
{
    if(isReady) setWindowTitle(tabWidget->tabText(index) + " - JustBrowser");
}

void MainWindow::downloadRequested(QWebEngineDownloadItem *download)
{
    QString path;
    if(settings->getDefaultDownloadPath() != "NULL") {
        QStringList downloadPath = download->path().split('/');
        if(!path.endsWith("/")) path += "/";
        path += downloadPath.at(downloadPath.count() - 1);
    }
    else {
        if(lastDownloadPath == "NULL") {
            path = download->path();
        }
        else {
            QStringList pathList = download->path().split("/");
            path = lastDownloadPath + pathList.at(pathList.count() - 1);
        }
        path = QFileDialog::getSaveFileName(this, "Save as...", path);
        QStringList pathList = path.split("/");
        lastDownloadPath.clear();
        for(int i = 0; i < pathList.count() - 1; i++) lastDownloadPath += pathList.at(i) + "/";
    }

    if(!path.isEmpty()) {
        download->setPath(path);
        download->accept();

        DownloadItem* item = new DownloadItem(path);
        ui->verticalLayout->addWidget(item);

        connect(download, &QWebEngineDownloadItem::downloadProgress, item, &DownloadItem::update);
        connect(item, &DownloadItem::pauseDownload, download, &QWebEngineDownloadItem::pause);
        connect(item, &DownloadItem::resumeDownload, download, &QWebEngineDownloadItem::resume);
        connect(item, &DownloadItem::stopDownload, download, &QWebEngineDownloadItem::cancel);

        connect(download, &QWebEngineDownloadItem::finished, [this, item, path, download] () {
            if (download->receivedBytes() == download->totalBytes()) {
                QMessageBox::information(this, "Download finished", "Download finished: " + path);
            }
            else {
                if (download->interruptReason() != QWebEngineDownloadItem::UserCanceled)
                    QMessageBox::information(this, "Download not finished", "Download " + path +
                                         " error: " + download->interruptReasonString());
            }
            ui->verticalLayout->removeWidget(item);
            item->~DownloadItem();
        });
    }
}
